use clap::ValueEnum;

/// HTML output format
#[derive(Clone, Debug, PartialEq, ValueEnum)]
pub enum HtmlOutputFormat {
    ByChapter,
    ByChapterStatic,
    SinglePage,
    SinglePageStatic,
}

impl Default for HtmlOutputFormat {
    fn default() -> Self {
        Self::ByChapter
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn html_output_format() {
        let hof = HtmlOutputFormat::ByChapter;
        assert_eq!(HtmlOutputFormat::ByChapter, hof);
        let hof = HtmlOutputFormat::ByChapterStatic;
        assert_eq!(HtmlOutputFormat::ByChapterStatic, hof);
        let hof = HtmlOutputFormat::SinglePage;
        assert_eq!(HtmlOutputFormat::SinglePage, hof);
        let hof = HtmlOutputFormat::SinglePageStatic;
        assert_eq!(HtmlOutputFormat::SinglePageStatic, hof);

        let default_hof = HtmlOutputFormat::default();
        assert_eq!(HtmlOutputFormat::ByChapter, default_hof);
    }
}
